using System.Threading;

using Android.App;
using Android.OS;

namespace ASSN3.Droid
{
    [Activity(Label = "SplashActivity", Icon ="@drawable/icon2", MainLauncher = true, NoHistory = false, Theme = "@style/MyTheme.Splash")]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Thread.Sleep(1000);

            StartActivity(typeof(MainActivity));
        }
    }
}