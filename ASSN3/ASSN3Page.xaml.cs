﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public class WebURL
	{
        [PrimaryKey, AutoIncrement]
		public int id { get; set; }
		public string url { get; set; }
		public string image { get; set; }
		public string title { get; set; }
	}

	public partial class ASSN3Page : ContentPage
	{
        private SQLiteAsyncConnection _connection;
        private List<WebURL> _urls;

		public ASSN3Page()
		{
			InitializeComponent();

            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
		}

		async void ReloadPicker()
		{
            await _connection.CreateTableAsync<WebURL>();
            _urls = await _connection.Table<WebURL>().ToListAsync();

			if (ShowURlS.Items.Count != 0)
			{
				ShowURlS.Items.Clear();
			}

			foreach (var x in _urls)
			{
				ShowURlS.Items.Add($"{x.title}");
			}
		}

		protected override void OnAppearing()
		{
			ReloadPicker();

			base.OnAppearing();
		}

		async void OpenWebView(object sender, System.EventArgs e)
		{
            if (ShowURlS.SelectedIndex > -1)
            {
                var passOnUrl = _urls[ShowURlS.SelectedIndex].url;
                await Navigation.PushModalAsync(new WebViewShow(passOnUrl));
            }
            else
            {
                await DisplayAlert("WEB Picker", "Please select a valid URL", "OK");
            }
		}

		async void AddURLAction(object sender, EventArgs e)
		{
			await Navigation.PushModalAsync(new AddURLPage());
		}

		async void UpdateURLAction(object sender, EventArgs e)
		{
            if (ShowURlS.SelectedIndex > -1)
            {
                _urls = await _connection.Table<WebURL>().ToListAsync();
                var selectedItems = ShowURlS.SelectedIndex;
                var selectedClass = urls[ShowURlS.SelectedIndex];

                await Navigation.PushModalAsync(new EditURLPage(selectedClass, selectedItems));
            }
            else
            {
                await DisplayAlert("Alert", "Please select a valid URL", "OK");
            }
		}

		async void RemoveURL(object sender, EventArgs e)
		{
            if (ShowURlS.SelectedIndex > -1)
            {
                _urls = await _connection.Table<WebURL>().ToListAsync();

                var selectedItems = _urls[ShowURlS.SelectedIndex];
                await _connection.DeleteAsync(selectedItems);

                ReloadPicker();
            }
            else
            {
                await DisplayAlert("Alert", "Please select a valid URL", "OK");
            }
        }
	}
}