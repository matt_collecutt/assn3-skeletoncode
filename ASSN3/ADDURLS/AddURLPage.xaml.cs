﻿using System;
using System.Threading.Tasks;

using SetupSQLite;

using SQLite;

using Xamarin.Forms;


namespace ASSN3
{
	public partial class AddURLPage : ContentPage
	{
		private SQLiteAsyncConnection _connection;

		public AddURLPage()
		{
			InitializeComponent();

            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
		}

		async void AddURL(object sender, EventArgs e)
		{
            if (await Validate())
            {
                var url = new WebURL
                {
                    title = TitleNew.Text,
                    image = ImageNew.Text,
                    url = URLNew.Text
                };

                await _connection.InsertAsync(url);

                await DisplayAlert("WEB Picker", "URL Added Successfully", "Ok");

                TitleNew.Text = "";
                ImageNew.Text = "";
                URLNew.Text = "";
            }
		}

		async void GoBack(object sender, EventArgs e)
		{
			await Navigation.PopModalAsync();
		}

        async Task<bool> Validate()
        {
            if (string.IsNullOrWhiteSpace(TitleNew.Text))
            {
                await DisplayAlert("WEB Picker", "Please enter a title", "OK");
                return false;
            }

            if (string.IsNullOrWhiteSpace(ImageNew.Text) && string.IsNullOrWhiteSpace(URLNew.Text))
            {
                await DisplayAlert("WEB Picker", "Please enter either an image URL or web URL", "OK");
                return false; 
            }

            return true;
        }

	}
}
