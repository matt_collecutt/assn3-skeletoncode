﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ASSN3
{
	public partial class WebViewShow : ContentPage
	{
		public WebViewShow(string url)
		{
			InitializeComponent();

            ShowWeb.Source = url;
		}

		async void GoBack(object sender, EventArgs e)
		{
			await Navigation.PopModalAsync();
		}
	}
}
